all: docker-image

docker-image: docker/Dockerfile docker/install
	docker build -t mawis/rust-cross-win docker

push:
	docker push mawis/rust-cross-win:latest
