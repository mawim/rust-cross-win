Environment to build windows binaries from Rust code
====================================================

This is a docker container, that can be used to build windows binaries from Rust
code.


Project setup
-------------

To be able to compile for windows, Cargo has to be told which linker and
archiver it can use. To configure this, a `.cargo/config` file in the project
folder is required.

It should contain (at least) the following section to configure where to find
the linker and archiver:

```
[target.x86_64-pc-windows-gnu]
linker = "/usr/bin/x86_64-w64-mingw32-gcc"
ar = "/usr/bin/x86_64-linux-gnu-ar"
```


Building the binary
-------------------

After configuration (see above) the project can be compiled for windows with the
following command:

```
cargo build --release --target x86_64-pc-windows-gnu
```


References
----------

* [Stack overflow: Cross-compile a Rust application from Linux to Windows](https://stackoverflow.com/questions/31492799/cross-compile-a-rust-application-from-linux-to-windows)
* [Stack overflow: Can't Get Cross Compiling From Ubuntu To Windows Working](https://stackoverflow.com/questions/56602101/i-cant-get-cross-compiling-from-ubuntu-to-windows-working)
